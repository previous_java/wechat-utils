package com.xyz.wex.course.util;

public class VarValid {
	public static boolean isValidOfString(String ... args){
		for (String string : args) {
			if(string==null||string.equals("")){
				return false ;
			}
		}
		return true;
	}
	public static boolean isValidOfLong(Long ... args){
		for (Long long1 : args) {
			if(long1==null||long1<=0){
				return false;
			}
		}
		return true;
	}
	public static boolean isValidOfInteger(Integer ... args){
		for (Integer int1 : args) {
			if(int1==null||int1<=0){
				return false;
			}
		}
		return true;
	}
}
