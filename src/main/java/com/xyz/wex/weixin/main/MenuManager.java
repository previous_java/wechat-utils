package com.xyz.wex.weixin.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xyz.wex.course.menu.Button;
import com.xyz.wex.course.menu.Menu;
import com.xyz.wex.course.menu.ViewButton;
import com.xyz.wex.course.pojo.Token;
import com.xyz.wex.course.util.CommonUtil;
import com.xyz.wex.course.util.MenuUtil;

/**
 * 菜单管理器类
 * 
 * @author liufeng
 * @date 2013-10-17
 */
//微信自定义菜单创建需要手写url项目名
public class MenuManager {
	private static Logger log = LoggerFactory.getLogger(MenuManager.class);

	/**
	 * 定义菜单结构
	 * 
	 * @return
	 */
	private static Menu getMenu() {
		/*ClickButton btn11 = new ClickButton();
		btn11.setName("开源中国");
		btn11.setType("click");
		btn11.setKey("oschina");

		ClickButton btn12 = new ClickButton();
		btn12.setName("ITeye");
		btn12.setType("click");
		btn12.setKey("iteye");*/

		ViewButton btn13 = new ViewButton();
		btn13.setName("会员中心");
		btn13.setType("view");
		btn13.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa6a51f9e8f6b84bd&redirect_uri=http://d.zzdxedp.com/drp/wxMember/center/1&response_type=code&scope=snsapi_base&state=1#wechat_redirect");

		/*ViewButton btn21 = new ViewButton();
		btn21.setName("淘宝");
		btn21.setType("view");
		btn21.setUrl("http://m.taobao.com");

		ViewButton btn22 = new ViewButton();
		btn22.setName("京东");
		btn22.setType("view");
		btn22.setUrl("http://m.jd.com");

		ViewButton btn23 = new ViewButton();
		btn23.setName("唯品会");
		btn23.setType("view");
		btn23.setUrl("http://m.vipshop.com");

		ViewButton btn24 = new ViewButton();
		btn24.setName("当当网");
		btn24.setType("view");
		btn24.setUrl("http://m.dangdang.com");

		ViewButton btn25 = new ViewButton();
		btn25.setName("苏宁易购");
		btn25.setType("view");
		btn25.setUrl("http://m.suning.com");

		ViewButton btn31 = new ViewButton();
		btn31.setName("多泡");
		btn31.setType("view");
		btn31.setUrl("http://www.duopao.com");*/

		ViewButton btn32 = new ViewButton();
		btn32.setName("会议助手");
		btn32.setType("view");
//		btn32.setUrl("http://d.zzdxedp.com/drp/user/center");
		btn32.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa6a51f9e8f6b84bd&redirect_uri=http://d.zzdxedp.com/drp/wxMeeting/center/1&response_type=code&scope=snsapi_base&state=123456#wechat_redirect");

		ViewButton btn31 = new ViewButton();
		btn31.setName("会议助手");
		btn31.setType("view");
//		btn32.setUrl("http://d.zzdxedp.com/drp/user/center");
		btn31.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf4cc1cd2151b1161&redirect_uri=http://d.zzdxedp.com/drp/wxMeeting/center/2&response_type=code&scope=snsapi_base&state=123456#wechat_redirect");

		/*ComplexButton mainBtn1 = new ComplexButton();
		mainBtn1.setName("我的会议");
		mainBtn1.setSub_button(new Button[] { btn13, btn32 });*/

		/*ComplexButton mainBtn2 = new ComplexButton();
		mainBtn2.setName("购物");
		mainBtn2.setSub_button(new Button[] { btn21, btn22, btn23, btn24, btn25 });

		ComplexButton mainBtn3 = new ComplexButton();
		mainBtn3.setName("网页游戏");
		mainBtn3.setSub_button(new Button[] { btn31, btn32 });*/

		Menu menu = new Menu();
		menu.setButton(new Button[] {/*btn13, */btn32});

		return menu;
	}

	public static void main(String[] args) {
		// 第三方用户唯一凭证
		String appId = "wxa6a51f9e8f6b84bd";
		// 第三方用户唯一凭证密钥
		String appSecret = "bb26cb1cc7960ae4f49fbb219f07ae5f";
		/*String appId = "wxf4cc1cd2151b1161";
		// 第三方用户唯一凭证密钥
		String appSecret = "eb638ae638576c40b4af5cd2bc8aec08";*/
		// 调用接口获取凭证
		Token token = CommonUtil.getToken(appId, appSecret);

		if (null != token) {
			// 创建菜单
			boolean result = MenuUtil.createMenu(getMenu(), token.getAccessToken());
//			boolean result = MenuUtil.createMenu(getMenu(), "oMxYLAp2AlDy1B8bk82-UP1MIXk1Qou773uwNpH4D7733yAYyQvvE9MqK4qMCwuqMPpD5o_132i2ud-7GlXX20FAorEcHYdk6vNgwcEPwacErldyl4HzMLxygLBl8PD9MOEeAEDQSI");

			// 判断菜单创建结果
			if (result)
				log.info("菜单创建成功！");
			else
				log.info("菜单创建失败！");
		}
	}
	public void mingding() {/*
		// 第三方用户唯一凭证
		String appId = "wx8b170bceba19e9cc";
		// 第三方用户唯一凭证密钥
		String appSecret = "2235151454e4ee00c8430d148d09b7b3";
		
		ClickButton btn11 = new ClickButton();
		btn11.setName("公司简介");
		btn11.setType("click");
		btn11.setKey("gongsijianjie");
//		btn11.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8b170bceba19e9cc&redirect_uri=http://d.zzdxedp.com/drp/publicNo/sendText/4?id=2&response_type=code&scope=snsapi_base&state=123456#wechat_redirect");
		ViewButton btn12 = new ViewButton();
		btn12.setName("精彩回放");
		btn12.setType("view");
		btn12.setUrl("http://mp.weixin.qq.com/s?__biz=MzIxNTUzMTMwMw==&mid=100000167&idx=1&sn=2fb30fe360234f780d072a14be5983f4&scene=18#wechat_redirect");
		ViewButton btn13 = new ViewButton();
		btn13.setName("近期线下活动");
		btn13.setType("view");
		btn13.setUrl("http://mp.weixin.qq.com/s?__biz=MzIxNTUzMTMwMw==&mid=100000014&idx=1&sn=e8d1e20df24b1e5fc0ab4290a9931b95&scene=18#wechat_redirect");
		ClickButton btn14 = new ClickButton();
		btn14.setName("业务合作");
		btn14.setType("click");
		btn14.setKey("yewuhezuo");
//		btn14.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8b170bceba19e9cc&redirect_uri=http://d.zzdxedp.com/drp/publicNo/sendText/4?id=1&response_type=code&scope=snsapi_base&state=123456#wechat_redirect");
		ViewButton btn21 = new ViewButton();
		btn21.setName("微课堂会员");
		btn21.setType("view");
		btn21.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=http://d.zzdxedp.com/drp/wxMember/center/4&response_type=code&scope=snsapi_base&state=123456#wechat_redirect");
		ViewButton btn22 = new ViewButton();
		btn22.setName("群加盟会员");
		btn22.setType("view");
		ViewButton btn23 = new ViewButton();
		btn23.setName("商城会员");
		btn23.setType("view");
		ViewButton btn24 = new ViewButton();
		btn24.setName("夺宝会员");
		btn24.setType("view");
		
		ViewButton btn31 = new ViewButton();
		btn31.setName("我要报名");
		btn31.setType("view");
//		btn32.setUrl("http://d.zzdxedp.com/drp/user/center");
		btn31.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx8b170bceba19e9cc&redirect_uri=http://d.zzdxedp.com/drp/wxMember/register/4&response_type=code&scope=snsapi_base&state=0&connect_redirect=1#wechat_redirect");

		ComplexButton mainBtn1 = new ComplexButton();
		mainBtn1.setName("走进我们");
		mainBtn1.setSub_button(new Button[] { btn11, btn12, btn13,  btn14});

		ComplexButton mainBtn2 = new ComplexButton();
		mainBtn2.setName("会员中心");
		mainBtn2.setSub_button(new Button[] { btn21, btn22, btn23, btn24 });

		ComplexButton mainBtn3 = new ComplexButton();
		mainBtn3.setName("我要报名");
		mainBtn3.setSub_button(new Button[] { btn31 });
		
		Menu menu = new Menu();
		menu.setButton(new Button[] {mainBtn1, mainBtn2, btn31});
		String appId = "wxf4cc1cd2151b1161";
		// 第三方用户唯一凭证密钥
		String appSecret = "eb638ae638576c40b4af5cd2bc8aec08";
		// 调用接口获取凭证
		Token token = CommonUtil.getToken(appId, appSecret);

		if (null != token) {
			// 创建菜单
			boolean result = MenuUtil.createMenu(menu, token.getAccessToken());
//					boolean result = MenuUtil.createMenu(getMenu(), "oMxYLAp2AlDy1B8bk82-UP1MIXk1Qou773uwNpH4D7733yAYyQvvE9MqK4qMCwuqMPpD5o_132i2ud-7GlXX20FAorEcHYdk6vNgwcEPwacErldyl4HzMLxygLBl8PD9MOEeAEDQSI");

			// 判断菜单创建结果
			if (result)
				log.info("菜单创建成功！");
			else
				log.info("菜单创建失败！");
		}
	*/}
	public void zhenzhicheng() {/*
		// 第三方用户唯一凭证
		String appId = "wx221429fb13f19bd1";
		// 第三方用户唯一凭证密钥
		String appSecret = "fcff0a1c1ee2798a1c8ead66f2e9168e";
		
		ViewButton btn13 = new ViewButton();
		btn13.setName("会议助手");
		btn13.setType("view");
		btn13.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=http://a.zzdxedp.com/drp/wxMeeting/center/5&response_type=code&scope=snsapi_base&state=5#wechat_redirect");
		Menu menu = new Menu();
		menu.setButton(new Button[] {btn13});
		System.out.println(btn13.getUrl());
		String appId = "wxf4cc1cd2151b1161";
		// 第三方用户唯一凭证密钥
		String appSecret = "eb638ae638576c40b4af5cd2bc8aec08";
		// 调用接口获取凭证
		Token token = CommonUtil.getToken(appId, appSecret);

		if (null != token) {
			// 创建菜单
			boolean result = MenuUtil.createMenu(menu, token.getAccessToken());
//					boolean result = MenuUtil.createMenu(getMenu(), "oMxYLAp2AlDy1B8bk82-UP1MIXk1Qou773uwNpH4D7733yAYyQvvE9MqK4qMCwuqMPpD5o_132i2ud-7GlXX20FAorEcHYdk6vNgwcEPwacErldyl4HzMLxygLBl8PD9MOEeAEDQSI");

			// 判断菜单创建结果
			if (result)
				log.info("菜单创建成功！");
			else
				log.info("菜单创建失败！");
		}
	*/}
	
	public void shangmaiwenhua() {
		// 第三方用户唯一凭证
		String appId = "wxd6109bbde7360543";
		// 第三方用户唯一凭证密钥
		String appSecret = "de9f688752b2662747a543548977f3be";
		
		ViewButton btn13 = new ViewButton();
		btn13.setName("会议助手");
		btn13.setType("view");
		btn13.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=http://d.zzdxedp.com/drp/wxMeeting/center/6&response_type=code&scope=snsapi_base&state=6#wechat_redirect");
		Menu menu = new Menu();
		menu.setButton(new Button[] {btn13});
		System.out.println(btn13.getUrl());
		/*String appId = "wxf4cc1cd2151b1161";
		// 第三方用户唯一凭证密钥
		String appSecret = "eb638ae638576c40b4af5cd2bc8aec08";*/
		// 调用接口获取凭证
		Token token = CommonUtil.getToken(appId, appSecret);

		if (null != token) {
			// 创建菜单
			boolean result = MenuUtil.createMenu(menu, token.getAccessToken());
//					boolean result = MenuUtil.createMenu(getMenu(), "oMxYLAp2AlDy1B8bk82-UP1MIXk1Qou773uwNpH4D7733yAYyQvvE9MqK4qMCwuqMPpD5o_132i2ud-7GlXX20FAorEcHYdk6vNgwcEPwacErldyl4HzMLxygLBl8PD9MOEeAEDQSI");

			// 判断菜单创建结果
			if (result)
				log.info("菜单创建成功！");
			else
				log.info("菜单创建失败！");
		}
	}
}
