package com.xyz;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.W3CDomHandler;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.render.FreeMarkerRender;

import freemarker.core.Environment;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
/**
 * 自定义指令，用来测试
 * @author zzp
 *
 */
public class FirstTag implements TemplateDirectiveModel {

	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		// TODO Auto-generated method stub
		List<?> list = Db.find("select * from g_ac_act");
		env.setVariable("list", FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(list));
		body.render(env.getOut());
	}
}
