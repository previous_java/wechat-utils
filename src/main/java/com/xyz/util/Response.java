package com.xyz.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回值对象
 * @author xyz
 *
 */
public class Response {

	public static final int SUCCESS = 0;
	public static final int LOGIN = 1;
	public static final int PRIVILEGE = 2;
	public static final int ERROR = 3;
	
	// 待转化为json的map对象
	static Map<String, Object> resp;
	public static Map<String, Object> getRespMap() {		
		resp = new HashMap<String, Object>();
		return resp;
	}
	// 已经设定好编码的map对象，成功
	public static Map<String, Object> getSuccRespMap() {
		resp = getRespMap();
		resp.put("code", SUCCESS);
		return resp;
	}
	// 已经设定好编码的map对象，错误
	public static Map<String, Object> getErrorRespMap() {
		resp = getRespMap();
		resp.put("code", ERROR);
		return resp;
	}
	// 已经设定好编码的map对象，传递一个参数
	public static Map<String, Object> getRespMap(int code) {
		resp = getRespMap();
		resp.put("code", code);
		return resp;
	}
	
	public static Map<String, Object> setContent(Object content) {
		resp.put("content", content);
		return resp;
	}
	
}