package com.xyz.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.jfinal.plugin.activerecord.Record;

public class ExportExcel {
	public static File sg(String fileName , String[] head , String[] key ,List<Record> content){
		HSSFWorkbook workbook = new HSSFWorkbook();//创建一个工作簿
		HSSFSheet sheet = workbook.createSheet();//创建一个表
		sheet.setDefaultColumnWidth((short) 19);//设置表单元格的宽度
		HSSFCellStyle style = workbook.createCellStyle();
        // 设置这些样式
        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 生成一个字体
        HSSFFont font = workbook.createFont();
        font.setColor(HSSFColor.VIOLET.index);
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        style.setFont(font);
        // 生成并设置另一个样式
        HSSFCellStyle style2 = workbook.createCellStyle();
        style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        // 生成另一个字体
        HSSFFont font2 = workbook.createFont();
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        font2.setFontName("UTF-8");
        // 把字体应用到当前的样式
        style2.setFont(font2);
        
		HSSFRow row = sheet.createRow(0);//创建第一行
		int len = Math.min(head.length, key.length);
		for (int i = 0; i < len; i++) {//把表头写入第一行
            HSSFCell cell = row.createCell(i);//创建单元格
            cell.setCellStyle(style);//设置单元格的样式
            cell.setCellValue(head[i]);//设置单元格的内容
        }
		for (int i = 0; i < content.size(); i++) {
			row = sheet.createRow(i+1);//创建第一行
			for (int j = 0; j < len ; j++) {
				HSSFCell cell = row.createCell(j);
				cell.setCellStyle(style2);
				cell.setCellValue(String.valueOf((content.get(i).get(key[j]))));
			}
		}
		 
		
		File file = null;
		try {
			file = File.createTempFile(fileName+(int)(Math.random()*10000000), ".xls");
			file.deleteOnExit();
			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			out.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
}
