package com.xyz.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.kit.HashKit;

public class Live {

	public static void main(String[] args) {
		String sign;
		// 当前utc时间戳
		Long timestamp = System.currentTimeMillis();
		// 暂时固定一个值
//		timestamp = 1462529653112L;
		// 将时间戳转化为指定格式的时间
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		System.out.println(sdf.format(timestamp));
		// 用md5加密：流名称、时间戳、签名密钥、"lecoud"
		sign = HashKit.md5("test" + timestamp + "95JGLEGWVVP3EDDWATAW" + "lecloud");
		
		// 最终生成的地址
		System.out.println("http://1965.mpull.live.lecloud.com/live/test/desc.m3u8?tm=" + timestamp
				+ "&sign=" + sign);
	}
}





/*20160506061413
dc8dd918edf7f36055c35f14b7805a15
*/