package com.xyz.util.auth;

import java.security.Key;

import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;

public class CopyOfAuthUtil {

//	private static final Key key = new AesKey(ByteUtil.randomBytes(16));
	private static final Key key = new AesKey(new byte[]{55, 52, 119, 53, 87, -81, -125, 55, -25, 5, 127, -74, -43, -110, 10, 96});
	private static JsonWebEncryption jwe;
	/**
	 * 获取加密串
	 * @throws JoseException 
	 */
	public static String getEncry(String info) throws JoseException {		
		jwe = new JsonWebEncryption();
		jwe.setPayload(info); // 主体信息
//		System.out.println(key);
		jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW); // 算法
		jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256); // 加密方法头参数
		jwe.setKey(key); // 随机数
//		System.out.println("key的值为：" + key.toString());
		String serializedJwe = jwe.getCompactSerialization();
		return serializedJwe;
	}
	public static String getDecry(String serializedJwe) throws JoseException {
		jwe = new JsonWebEncryption();
		jwe.setKey(key);
		jwe.setCompactSerialization(serializedJwe);
		return jwe.getPayload();
	}
	
	public static void main(String[] args) throws JoseException {
	}
}
