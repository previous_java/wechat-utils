/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.xyz.weixin.sdk.api;

import com.xyz.weixin.sdk.utils.HttpUtils;

/**
 * menu api
 */
public class MenuApi {
	
	private static String getMenu = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=";
	private static String createMenu = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";
	
	/**
	 * 查询菜单
	 */
	public static ApiResult getMenu(String appId) {
		String jsonResult = HttpUtils.get(getMenu + AccessTokenApi.getAccessTokenStr(appId));
		return new ApiResult(jsonResult, appId);
	}
	
	/**
	 * 创建菜单
	 */
	public static ApiResult createMenu(String jsonStr, String appId) {
		String jsonResult = HttpUtils.post(createMenu + AccessTokenApi.getAccessTokenStr(appId), jsonStr);
		return new ApiResult(jsonResult, appId);
	}
}


